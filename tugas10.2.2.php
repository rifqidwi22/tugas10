<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Berkalang</title>
</head>
<body>
    <pre>
        <?php
        $tinggi = isset($_GET['tinggi']) ? intval($_GET['tinggi']) : 5;

        for ($baris = 0; $baris <= $tinggi; $baris++) {
            
            for ($i = 1; $i <= $tinggi - $baris; $i++) {
                echo "&nbsp;"; 
            }
            
            for ($j = 1; $j <= 2 * $baris - 1; $j++) {
                echo "*";
            }
            
            echo "<br>";
        }
        ?>
    </pre>
</body>
</html>
